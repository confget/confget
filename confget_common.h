#ifndef _INCLUDED_CONFGET_COMMON_H
#define _INCLUDED_CONFGET_COMMON_H

/*-
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

void			 common_openfile(void);
void			 common_openfile_null(void);
void			 common_closefile(void);

char			*confgetline(FILE *_fp, char **_line, size_t *_len);

#endif /* _INCLUDED_CONFGET_COMMON_H */
