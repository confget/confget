# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

LOCALBASE?=	/usr/local
PREFIX?=	${LOCALBASE}
BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/man/man
EXAMPLESDIR?=	${PREFIX}/share/examples/${PROG}

STD_CPPFLAGS?=	-D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=700

LFS_CPPFLAGS?=	-D_FILE_OFFSET_BITS=64
LFS_LDFLAGS?=

# To use the PCRE2 library (default):
PCRE_CPPFLAGS?=	-DHAVE_PCRE2 -I${LOCALBASE}/include
PCRE_LIBS?=	-L${LOCALBASE}/lib -lpcre2-8

# To use the obsolete pcre library:
# PCRE_CPPFLAGS?=	-DHAVE_PCRE -I${LOCALBASE}/include
# PCRE_LIBS?=	-L${LOCALBASE}/lib -lpcre

STD_CFLAGS?=	-std=c99
WARN_CFLAGS?=	-Wall -W

CC?=		gcc
CPPFLAGS?=
CPPFLAGS+=	${STD_CPPFLAGS} ${LFS_CPPFLAGS} ${PCRE_CPPFLAGS}
CFLAGS?=		-g -pipe
CFLAGS+=	${STD_CFLAGS} ${WARN_CFLAGS}
LDFLAGS?=
LDFLAGS+=	${LFS_LDFLAGS}
LIBS?=

RM=		rm -f
MKDIR?=		mkdir -p
SETENV?=	env

PROG=		confget
OBJS=		confget.o confget_common.o confget_ini.o
SRCS=		confget.c confget_common.c confget_ini.c
DEPENDFILE=	.depend

MAN1=		confget.1
MAN1GZ=		${MAN1}.gz

BINOWN?=	root
BINGRP?=	wheel
BINMODE?=	555

SHAREOWN?=	${BINOWN}
SHAREGRP?=	${BINGRP}
SHAREMODE?=	644

INSTALL?=	install
COPY?=		-c
STRIP?=		-s

INSTALL_PROGRAM?=	${INSTALL} ${COPY} ${STRIP} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_SCRIPT?=	${INSTALL} ${COPY} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA?=	${INSTALL} ${COPY} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}

# development/debugging flags, you may safely ignore them
BDECFLAGS=	-W -Wall -std=c99 -pedantic -Wbad-function-cast -Wcast-align \
		-Wcast-qual -Wchar-subscripts -Winline \
		-Wmissing-prototypes -Wnested-externs -Wpointer-arith \
		-Wredundant-decls -Wshadow -Wstrict-prototypes -Wwrite-strings
#CFLAGS+=	${BDECFLAGS}
#CFLAGS+=	-ggdb -g3

TESTEXEC=	test-getline test-fgetln

all:		${PROG}
		[ -n '${NO_DOC_BUILD}' ] || ${MAKE} ${MAN1GZ}

clean:
		${RM} ${PROG} ${OBJS} ${MAN1GZ}
		${RM} ${TESTEXEC} readaline.h

${PROG}:	${OBJS}
		${CC} ${LDFLAGS} -o ${PROG} ${OBJS} ${PCRE_LIBS}

.c.o:
		${CC} ${CPPFLAGS} ${CFLAGS} -c $<

${MAN1GZ}:	${MAN1}
		gzip -c9 ${MAN1} > ${MAN1GZ}.tmp
		mv ${MAN1GZ}.tmp ${MAN1GZ}

install:	all install-bin
		[ -n '${NO_DOC_BUILD}' ] || ${MAKE} install-man install-examples

install-bin:
		-${MKDIR} ${DESTDIR}${BINDIR}
		${INSTALL_PROGRAM} ${PROG} ${DESTDIR}${BINDIR}/

install-man:
		-${MKDIR} ${DESTDIR}${MANDIR}1
		${INSTALL_DATA} ${MAN1GZ} ${DESTDIR}${MANDIR}1/

install-examples:
		-${MKDIR} ${DESTDIR}${EXAMPLESDIR}/tests
		${INSTALL_SCRIPT} t/*.t ${DESTDIR}${EXAMPLESDIR}/tests/
		${INSTALL_DATA} t/*.ini ${DESTDIR}${EXAMPLESDIR}/tests/

depend:
		touch readaline.h
		${SETENV} CPPFLAGS="-DCONFGET_MAKEDEP ${CPPFLAGS}" \
		DEPENDFILE="${DEPENDFILE}" sh makedep.sh ${SRCS}
		rm -f readaline.h
		
test:		all
		prove t

readaline.h:	test-getline.c Makefile
		rm -f readaline.h
		if ${CC} ${CPPFLAGS} ${CFLAGS} ${LDFLAGS} -o test-getline test-getline.c ${LIBS}; then \
			echo '#define HAVE_GETLINE' > readaline.h; \
		else if ${CC} ${CPPFLAGS} ${CFLAGS} ${LDFLAGS} -o test-fgetln test-fgetln.c ${LIBS}; then \
			echo '#define HAVE_FGETLN' > readaline.h; \
		else \
			echo 'Neither getline() nor fgetln() found!' 1>&2; \
			false; \
		fi; fi

.PHONY:		all clean depend install test

ifeq ($(wildcard ${DEPENDFILE}),${DEPENDFILE})
include ${DEPENDFILE}
endif
