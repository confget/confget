/*-
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
	char *line;
	size_t n;

	line = fgetln(stdin, &n);
	if (line == NULL) {
		if (ferror(stdin))
			perror("Trying to read a line");
		else
			puts("No data read");
	} else {
		size_t i;

		printf("%zu 0 ", n);
		for (i = 0; i < n; i++)
			putchar(*line++);
	}
	return (0);
}
