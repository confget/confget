.\" SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
.\" SPDX-License-Identifier: BSD-2-Clause
.\"
.Dd May 9, 2021
.Dt CONFGET 1
.Os
.Sh NAME
.Nm confget
.Nd read a variable from a configuration file
.Sh SYNOPSIS
.Nm
.Op Fl cOSx
.Op Fl N | Fl n
.Op Fl f Ar filename
.Op Fl m Ar pattern
.Op Fl P Ar postfix
.Op Fl p Ar prefix
.Op Fl s Ar section
.Op Fl t Ar type
.Ar varname...
.Nm
.Op Fl OSx
.Op Fl N | Fl n
.Op Fl f Ar filename
.Op Fl m Ar pattern
.Op Fl P Ar postfix
.Op Fl p Ar prefix
.Op Fl s Ar section
.Op Fl t Ar type
.Fl L Ar pattern...
.Nm
.Op Fl OSx
.Op Fl N | Fl n
.Op Fl f Ar filename
.Op Fl m Ar pattern
.Op Fl P Ar postfix
.Op Fl p Ar prefix
.Op Fl s Ar section
.Op Fl t Ar type
.Fl l
.Nm
.Op Fl f Ar filename
.Fl q Li sections
.Op Fl t Ar type
.Nm
.Op Fl hTV
.Sh DESCRIPTION
The
.Nm
utility examines a INI-style configuration file and retrieves the value of
the specified variables from the specified section.
Its intended use is to let shell scripts use the same INI-style configuration
files as other programs, to avoid duplication of data.
.Pp
The
.Nm
utility may retrieve the values of one or more variables, list all
the variables in a specified section, list only those whose names or
values match a specified pattern (shell glob or regular expression), or
check if a variable is present in the file at all.
It has a
.Dq shell-quoting
output mode that quotes the variable values in a way suitable for passing
them directly to a Bourne-style shell.
.Pp
Options:
.Bl -tag -width indent
.It Fl c
Check-only mode; exit with a code of 0 if any of the variables are present in
the configuration file, and 1 if there are none.
.It Fl f Ar filename
Specify the configuration file to read from, or
.Dq -
(a single dash) for standard input.
.It Fl h
Display program usage information and exit.
.It Fl L
Variable list mode; display the names and values of all variables in the
specified section with names matching one or more specified patterns.
.It Fl l
List mode; display the names and values of all variables in the specified
section.
.It Fl m Ar pattern
Only display variables with if their values match the specified pattern.
.It Fl N
Always display the variable name along with the value.
.It Fl n
Never display the variable name, only the value.
.It Fl O
Allow variables in the specified section to override variables in
the unnamed section at the start of the file.
This is the only case when
.Nm
even considers variables in more than one section.
.It Fl P Ar postfix
Display this string after the variable name as a postfix.
.It Fl p Ar prefix
Display this string before the variable name as a prefix.
.It Fl q Ar query
Query for a specific type of information.
For the present, the only supported value for the
.Ar query
argument is
.Dq sections ,
which lists the names of the sections defined in the configuration file.
.It Fl S
Quote the variable values so that the
.Dq var=value
lines may be passed directly to the Bourne shell.
.It Fl s Ar section
Specify the configuration section to read.
If this option is specified,
.Nm
will only consider variables defined in the specified section;
see the
.Fl O
option for the only exception.
.Pp
If this option is not specified,
.Nm
will use the first section found in the configuration file.
However, if the configuration file contains variable definitions before
a section header,
.Nm
will only examine them instead.
.Pp
If the
.Fl s
option is specified with an empty string as the section name,
.Nm
will
.Em only
examine variables defined before any section header
.Po
a
.Dq real
unnamed default section
.Pc ;
this is incompatible with the
.Fl O
option.
.It Fl T
List the available configuration file types that may be selected by the
.Fl t
option.
.It Fl t Ar type
Specify the configuration file type.
.It Fl V
Display program version information and exit.
.It Fl x
Treat the patterns as regular expressions instead of shell glob patterns.
.El
.Sh ENVIRONMENT
Not taken into consideration.
.Sh EXIT STATUS
If the
.Fl c
option is specified, the
.Nm
utility will exit with a status of 0 if any of the specified variables exist in
the config file and 1 if none of them are present.
.Pp
In normal operation, no matter whether any variables were found in
the configuration file or not,
the
.Nm
utility exits with a status of 0 upon normal completion.
If any errors should occur while accessing or parsing the configuration file,
the
.Nm
utility will display a diagnostic message on the standard error stream and
exit with a status of 1.
.Sh EXAMPLES
Retrieve the variable
.Dv machine_id
from the
.Dv system
section of a configuration file:
.Pp
.Dl confget -f h.conf -s system machine_id
.Pp
Retrieve the variable
.Dv hostname
from the
.Dv db
section, but only if it ends in
.Dq .ringlet.net :
.Pp
.Dl confget -f h.conf -s db -m '*.ringlet.net' hostname
.Pp
Display the names and values of all variables declared before
any section has been defined:
.Pp
.Dl confget -f h.conf -s '' -l
.Pp
Display the names and values of all variables in the
.Dv system
section with names beginning with
.Dq mach
or ending in
.Dq name ,
appending a
.Dq cfg_
at the start of each variable name:
.Pp
.Dl confget -f h.conf -s system -p 'cfg_' -L 'mach*' '*name'
.Pp
Display the names and values of all variables in the
.Dv system
section:
.Pp
.Dl confget -f h.conf -s system -l
.Pp
Safely read the contents of the
.Dv db
section:
.Pp
.Dl eval `confget -f h.conf -s db -p db_ -S -l`
.Sh SEE ALSO
For another way to parse INI files, see the
.Xr Config::IniFiles 3
Perl module.
.Sh STANDARDS
No standards documentation was harmed in the process of creating
.Nm .
.Sh BUGS
Please report any bugs in
.Nm
to the author.
.Sh AUTHOR
The
.Nm
utility was conceived and written by
.An Peter Pentchev Aq roam@ringlet.net
in 2008.
