/*-
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
	char *line;
	size_t n;
	ssize_t res;

	line = NULL;
	n = 0;
	res = getline(&line, &n, stdin);
	if (res == -1) {
		if (ferror(stdin))
			perror("Trying to read a line");
		else
			puts("No data read");
	} else {
		printf("%zu %zd %s", n, res, line);
		free(line);
	}
	return (0);
}
