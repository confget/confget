<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

This is confget, a simple utility for parsing configuration files and
extracting values from them.  It currently parses INI-style files only,
but support for other formats may come at some point in the future.

The confget utility is mainly intended to be used in shell scripts for
fetching values out of configuration files.

To compile confget, your standard C library must provide one of two
functions: either getline(3) on Linux or other systems or fgetln(3) on
BSD-like systems.  The confget build procedure should be able to
autodetect the presence of a suitable line reading function; if it fails
on your platform, please report this to the author!

You may need to tweak the PCRE_CPPFLAGS and PCRE_LIBS variables to
indicate the presence or lack of PCRE support, the location of
the pcre2.h header file, and the name and location of the libpcre2-8.so
library itself (or, if still using pcre instead of pcre2, the pcre.h
header file and the libpcre.so library).

Comments: Peter Pentchev <roam@ringlet.net>
