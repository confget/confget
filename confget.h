#ifndef __INCLUDED_CONFGET_H
#define __INCLUDED_CONFGET_H

/*-
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#ifndef __unused
#ifdef __GNUC__
#define __unused __attribute__((unused))
#else  /* __GNUC__ */
#define __unused
#endif /* __GNUC__ */
#endif /* __unused */

#define VERSION_STRING	"5.1.3"

typedef struct {
	const char	* const name;
	void		(* const openfile)(void);
	void		(* const readfile)(void);
	void		(* const closefile)(void);
} confget_backend;

/* The currently processed input file */
extern FILE		*conffile;

/* The configuration filename specified by the user. */
extern const char	*filename;
/* The configuration section specified by the user. */
extern const char	*section;

/* Should section variables override default ones? */
extern bool		 override;
/* Query the section names? */
extern bool		 qsections;

void			 foundvar(const char * const _sec,
				const char * const _name,
				const char * const _value);
void			 foundsection(const char * const _sec);

#endif /* _INCLUDED */
