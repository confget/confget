/*-
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <ctype.h>
#include <err.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "confget.h"
#include "confget_common.h"
#include "confget_ini.h"

static void		 readfile_ini(void);

confget_backend	 confget_ini_backend = {
	"ini",
	common_openfile,
	readfile_ini,
	common_closefile,
};

#define RF_SKIPSPACE	while (*p != '\0' && isspace(*p)) p++;
#define RF_SKIPNONSPACE	while (*p != '\0' && *p != '=' && !isspace(*p)) p++;

/***
 * Function:
 *	readfile_ini		- scan an INI file for the requested variable
 * Inputs:
 *	None.
 * Returns:
 *	Nothing; exits on error.
 * Modifies:
 *	May write to standard output if the variable has been found.
 *	May write to standard error and terminate the program.
 */
static void
readfile_ini(void)
{
	char *grp = NULL;
	bool ingroup = false, incont = false, seenvars = false;
	char *vname = NULL, *vvalue = NULL;
	if (section == NULL || section[0] == '\0' || override)
		ingroup = true;
	size_t len = 0;
	char *line = NULL;
	while (confgetline(conffile, &line, &len) != NULL) {
		char *p = line;
		if (incont) {
			if (vvalue == NULL)
				errx(2, "INTERNAL ERROR: null vvalue incont");
			size_t vlen = strlen(vvalue);
			const size_t plen = strlen(p);
			vvalue = realloc(vvalue, vlen + plen + 1);
			if (vvalue == NULL)
				err(1, "Out of memory for the variable value");
			memcpy(vvalue + vlen, p, plen);
			vvalue[vlen + plen] = '\0';

			vlen += plen;
			if (vlen > 0 && vvalue[vlen - 1] == '\\') {
				vvalue[vlen - 1] = '\0';
				continue;
			}
			incont = false;
			while (vlen > 0 && isspace(vvalue[vlen - 1]))
				vvalue[--vlen] = '\0';

			if (ingroup) {
				seenvars = true;
				foundvar(grp, vname, vvalue);
				free(vname);
				free(vvalue);
				vname = vvalue = NULL;
			}
			continue;
		}
		RF_SKIPSPACE;
		/* Comment? */
		if (*p == '#' || *p == ';' || *p == '\0')
			continue;
		/* Section? */
		if (*p == '[') {
			if (grp != NULL) {
				free(grp);
				grp = NULL;
			}
			p++;
			RF_SKIPSPACE;
			if (*p == '\0')
				errx(1, "Invalid group line: %s", line);
			const char * const start = p++;
			char *end = p;
			while (*p != '\0' && *p != ']') {
				if (!isspace(*p))
					end = p + 1;
				p++;
			}
			if (*p != ']')
				errx(1, "Invalid group line: %s", line);
			p++;
			RF_SKIPSPACE;
			if (*p != '\0')
				errx(1, "Invalid group line: %s", line);

			*end = '\0';
			if (section == NULL) {
				if (seenvars && !qsections) {
					free(line);
					return;
				}
				section = strdup(start);
				if (section == NULL)
					errx(1, "Out of memory for the "
					    "group name");
			}

			grp = strdup(start);
			if (grp == NULL)
				err(1, "Out of memory for the group name");
			foundsection(grp);

			ingroup = strcmp(grp, section) == 0;
		} else {
			/* Variable! */
			if (vname != NULL) {
				free(vname);
				vname = NULL;
			}
			if (vvalue != NULL) {
				free(vvalue);
				vvalue = NULL;
			}
			const char * const start = p++;
			RF_SKIPNONSPACE;
			if (*p == '\0')
				errx(1, "No value on line: %s", line);
			char *end = p;
			RF_SKIPSPACE;
			if (*p != '=')
				errx(1, "No value on line: %s", line);
			p++;
			RF_SKIPSPACE;

			*end = '\0';
			vname = strdup(start);
			if (vname == NULL)
				err(1, "Out of memory for the variable name");
			vvalue = strdup(p);
			if (vvalue == NULL)
				err(1, "Out of memory for the variable value");

			size_t vlen = strlen(vvalue);
			if (vlen > 0 && vvalue[vlen - 1] == '\\') {
				vvalue[vlen - 1] = '\0';
				incont = true;
				continue;
			}
			while (vlen > 0 && isspace(vvalue[vlen - 1]))
				vvalue[--vlen] = '\0';

			if (ingroup) {
				seenvars = true;
				foundvar(grp, vname, vvalue);
				free(vname);
				free(vvalue);
				vname = vvalue = NULL;
			}
		}
	}
	free(grp);
	free(vname);
	free(vvalue);
	free(line);
	if (!feof(conffile))
		err(1, "Error reading input file %s", filename);
	if (incont)
		errx(1, "Invalid input file - continuation at the last line");
}
