<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# confget - parse configuration files

The `confget` library parses configuration files (currently INI-style
files only) and allows a program to use the values defined in them.
It provides various options for selecting the variable names and
values to return and the configuration file sections to fetch them from.

The `confget` library may also be used as a command-line tool with
the same interface as the C implementation.

## Quick-and-easy parsing of INI-style files

The `read_ini_file()` function will parse
an INI-style file and return a hashmap of the values found in its
various sections, as well as the name of the first section found in
the file (or the one specified in the configuration if
the `section_override` option
is enabled).

    use confget;
    
    // Default configuration except for the filename.
    let config = confget::Config {
        filename: Some("/etc/config.ini".to_string()),
        ..confget::Config::default()
    };
    let data, first_section = confget::read_ini_file(&config)?;

For the crate's change history, see
the [CHANGES](https://gitlab.com/confget/confget/-/blob/master/CHANGES)
file in the source distribution.

Author: Peter Pentchev <[roam@ringlet.net](mailto:roam@ringlet.net)>
