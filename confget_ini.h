#ifndef __INCLUDED_CONFGET_INI_H
#define __INCLUDED_CONFGET_INI_H

/*-
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

extern confget_backend	 confget_ini_backend;

#endif /* _INCLUDED */
