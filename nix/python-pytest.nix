# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

{ pkgs ? import <nixpkgs> { }
, py-ver ? 311
}:
let
  python-name = "python${toString py-ver}";
  python = builtins.getAttr python-name pkgs;
  python-with-pytest = python.withPackages (p: with p; [ pyparsing pytest ]);
in
pkgs.mkShell {
  buildInputs = [ python-with-pytest ];

  shellHook = ''
    set -e
    cd python
    env PYTHONPATH="$(pwd)/src" pytest -s unit_tests
    exit
  '';
}
