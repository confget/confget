# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

{ pkgs ? import <nixpkgs> { }
}:
pkgs.mkShell {
  buildInputs = [
    pkgs.groff
    pkgs.pcre2
    pkgs.perl
  ];

  buildFlags = "-DHAVE_PCRE2";

  shellHook = ''
    set -e
    make clean
    make PCRE_CPPFLAGS='-DHAVE_PCRE2' PCRE_LIBS='-lpcre2-8'
    make test
    exit
  '';
}
