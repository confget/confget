#!/bin/sh
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

venv='/opt/confget-tox'

echo '===> Creating the virtual environment for Tox'
python3 -m venv -- "$venv"

echo '===> Updating the build tools in the virtual environment'
"$venv/bin/python3" -m pip install -U pip setuptools

echo '===> Installing Tox into the virtual environment'
"$venv/bin/python3" -m pip install tox

echo '===> Making sure Tox works'
"$venv/bin/tox" --version
"$venv/bin/tox" --version | grep -Eqe '^[34]\.'
