#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'
[ -z "$TESTDIR" ] && TESTDIR='t'

T1="$TESTDIR/t1.ini"
T2="$TESTDIR/t2.ini"

echo '1..2'

if [ ! -f "$T1" ]; then
	echo "Bail out!  No test file $T1"
	exit 255
fi
if [ ! -f "$T2" ]; then
	echo "Bail out!  No test file $T2"
	exit 255
fi

test_1_check_sections()
{
        local v=''

        while [ "$#" -gt 0 ]; do
                read v
                if [ "$v" != "$1" ]; then
                        echo "not ok 1 expected '$1' got '$v'"
                        return
                fi
                shift
        done

        read v
        if [ -n "$v" ]; then
                echo "not ok 1 expected empty got '$v'"
        else
                echo 'ok 1'
        fi
}

# OK, this is so ugly it hurts...
$CONFGET -f "$T1" -q sections 2>&1 | test_1_check_sections 'a' 'b sect' 'c' 'longcont'

v=`$CONFGET -f "$T2" -q sections 2>&1`
if [ "$v" = 'sec1' ]; then echo 'ok 2'; else echo "not ok 2 v is $v"; fi
