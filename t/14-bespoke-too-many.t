#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'
[ -z "$TESTDIR" ] && TESTDIR='t'

T1="$TESTDIR/t1.ini"
T2="$TESTDIR/t2.ini"

echo '1..13'

if [ ! -f "$T1" ]; then
	echo "Bail out!  No test file $T1"
	exit 255
fi
if [ ! -f "$T2" ]; then
	echo "Bail out!  No test file $T2"
	exit 255
fi

idx=1
for args in \
    '-l -L k' \
    '-l -q sections' \
    '-l -q features' \
    '-l -q feature BASE' \
    '-l key1' \
    '-L -q sections k' \
    '-L -q features k' \
    '-L -q feature BASE k' \
    '-q sections -q features' \
    '-q sections -q feature BASE' \
    '-q sections key1' \
    '-q features -q feature BASE' \
    '-q features key1'; do
	if [ "${args#-q sections -q}" != "$args" ] || [ "${args#-q features -q}" != "$args" ]; then
		if [ "${CONFGET#*python}" != "$CONFGET" ] || [ "${CONFGET#*pypy}" != "$CONFGET" ]; then
			echo "ok $idx $args - skipped, Python argparse"
			idx="$((idx + 1))"
			continue
		fi
	fi
	v=`$CONFGET -f "$T2" $args 2>&1`
	if expr "x$v" : 'x.*Only a single ' > /dev/null || expr "x$v" : 'x.*provided more than once' || expr "x$v" : 'x.*used multiple times' > /dev/null; then echo "ok $idx $args"; else echo "not ok $idx args $args v is $v"; fi
	idx="$((idx + 1))"
done
