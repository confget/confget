#!/bin/sh
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'
[ -z "$TESTDIR" ] && TESTDIR='t'

echo '1..9'


if [ ! -f "$TESTDIR/t1.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t1.ini"
        exit 255
fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a '-m' 'value*' 'key1' `
res="$?"
if [ "$v" = value1 ]; then echo 'ok 1'; else echo "not ok 1 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a '-m' '*ue2' 'key2' `
res="$?"
if [ "$v" = value2 ]; then echo 'ok 2'; else echo "not ok 2 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a '-m' '*val'"'"'ue*' 'key3' `
res="$?"
if [ "$v" = '		 val'"'"'ue3' ]; then echo 'ok 3'; else echo "not ok 3 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' 'b sect' '-m' '*alu*' 'key4' `
res="$?"
if [ "$v" = 'v'"'"'alu'"'"'e4' ]; then echo 'ok 4'; else echo "not ok 4 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' c 'key5' `
res="$?"
if [ "$v" = '		# value5' ]; then echo 'ok 5'; else echo "not ok 5 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a '-m' '*alu*' 'key1' 'key2' | tr "\n" " "`
res="$?"
if [ "$v" = 'key1=value1 key2=value2 ' ]; then echo 'ok 6'; else echo "not ok 6 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a '-m' '*7' 'key6' `
res="$?"
if [ "$v" = '' ]; then echo 'ok 7'; else echo "not ok 7 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' 'b sect' '-c' '-m' '*7' 'key7' `
res="$?"
if [ "$res" = 0 ]; then echo 'ok 8'; else echo "not ok 8 res is '$res'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' 'b sect' '-c' '-m' '*7' 'key6' `
res="$?"
if [ "$res" != 0 ]; then echo 'ok 9'; else echo "not ok 9 res is '$res'"; fi
