#!/bin/sh
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'
[ -z "$TESTDIR" ] && TESTDIR='t'

echo '1..5'


if [ ! -f "$TESTDIR/t1.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t1.ini"
        exit 255
fi

if [ ! -f "$TESTDIR/t4.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t4.ini"
        exit 255
fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a '-l' | wc -l | tr -d ' '`
res="$?"
if [ "$v" = 4 ]; then echo 'ok 1'; else echo "not ok 1 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' 'b sect' '-l' '-N' | fgrep -e = | wc -l | tr -d ' '`
res="$?"
if [ "$v" = 2 ]; then echo 'ok 2'; else echo "not ok 2 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' c '-l' '-n' | fgrep -ve = | wc -l | tr -d ' '`
res="$?"
if [ "$v" = 1 ]; then echo 'ok 3'; else echo "not ok 3 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' d '-l' | wc -l | tr -d ' '`
res="$?"
if [ "$v" = 0 ]; then echo 'ok 4'; else echo "not ok 4 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t4.ini" '-s' x '-l' `
res="$?"
if [ "$v" = key8=key9=key10=key11 ]; then echo 'ok 5'; else echo "not ok 5 v is '$v'"; fi
