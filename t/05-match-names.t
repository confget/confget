#!/bin/sh
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'
[ -z "$TESTDIR" ] && TESTDIR='t'

echo '1..4'


if [ ! -f "$TESTDIR/t1.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t1.ini"
        exit 255
fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a '-L' '*' | wc -l | tr -d ' '`
res="$?"
if [ "$v" = 4 ]; then echo 'ok 1'; else echo "not ok 1 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' 'b sect' '-L' '-N' '*' | fgrep -e = | wc -l | tr -d ' '`
res="$?"
if [ "$v" = 2 ]; then echo 'ok 2'; else echo "not ok 2 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a '-L' '-n' '*ey2' | fgrep -ve = | wc -l | tr -d ' '`
res="$?"
if [ "$v" = 1 ]; then echo 'ok 3'; else echo "not ok 3 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' d '-L' '*ey2' | wc -l | tr -d ' '`
res="$?"
if [ "$v" = 0 ]; then echo 'ok 4'; else echo "not ok 4 v is '$v'"; fi
