#!/usr/bin/python3
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

"""Generate shell scripts with TAP output for the confget tests."""

# Maybe someday we shall deserialize the test definitions file. Until then, well...
# pylint: disable=magic-value-comparison

import pathlib
import shlex
from typing import List

from unit_tests.data import test_defs as t_defs
from unit_tests.data import test_load as t_load


PRELUDE = r"""#!/bin/sh
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'
[ -z "$TESTDIR" ] && TESTDIR='t'

echo '1..{count}'
"""

CHECK_TESTFILE = r"""
if [ ! -f "$TESTDIR/{fname}" ]; then
        echo "Bail out!  No test file $TESTDIR/{fname}"
        exit 255
fi"""


def add_cmdline_options(cmdline: List[str], test: t_defs.SingleTestDef) -> None:
    """Add the options from test.args into cmdline."""
    for name, value in test.args.items():
        option = t_defs.CMDLINE_OPTIONS[name]
        if not option[0]:
            continue
        cmdline.append(f"'{option[0]}'")
        if option[1]:
            if name == "filename":
                cmdline.append(f'"$TESTDIR/{value}"')
            else:
                cmdline.append(shlex.quote(value))


def main() -> None:
    """Generate the test files."""
    tests = t_load.load_all_tests(".")
    for fname, data in sorted(tests.items()):
        print(f"Generating {fname}.t with {len(data.tests)} tests")
        with pathlib.Path(f"{fname}.t").open(mode="w", encoding="UTF-8") as testf:
            print(PRELUDE.format(count=len(data.tests)), file=testf)

            filenames = sorted(
                {test.args["filename"] for test in data.tests if "filename" in test.args},
            )
            for filename in filenames:
                print(CHECK_TESTFILE.format(fname=filename), file=testf)

            for idx, test in enumerate(data.tests):
                tap_idx = idx + 1

                cmdline: List[str] = []

                cmdline.append("$CONFGET")
                if test.backend != "ini":
                    cmdline.extend(["-t", test.backend])

                add_cmdline_options(cmdline, test)

                if test.stdin is not None:
                    cmdline.extend([t_defs.CMDLINE_OPTIONS["filename"][0], "-"])

                cmdline.extend([f"'{key}'" for key in test.keys])

                if test.stdin is not None:
                    cmdline.extend(["<", f'"$TESTDIR/{test.stdin}"'])

                xform = t_defs.XFORM[test.xform].command
                print(f'- {tap_idx}: {" ".join(cmdline)} {xform}')

                print(f'v=`{" ".join(cmdline)} {xform}`', file=testf)
                print('res="$?"', file=testf)

                check = test.output.get_check()
                print(f"  - {check}")
                print(
                    f"if {check}; then echo 'ok {tap_idx}'; else "
                    f'echo "not ok {tap_idx} {test.output.var_name} is '
                    f"'${test.output.var_name}'\"; fi",
                    file=testf,
                )


if __name__ == "__main__":
    main()
