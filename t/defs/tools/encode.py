#!/usr/bin/python3
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

"""Encode a Python confget test data structure into a JSON file."""

import json

from typing import Any, Dict

from unit_tests.data import data as t_data
from unit_tests.data import defs as t_defs


class TestEncoder(json.JSONEncoder):
    """ Encode the confget test data into serializable objects. """

    def encode_test_file_def(self, obj: t_defs.TestFileDef) -> Dict[str, Any]:
        """ Encode a full TestFileDef object. """
        return {
            'format': {
                'version': {
                    'major': 1,
                    'minor': 0,
                },
            },
            'setenv': obj.setenv,
            'tests': [self.default(test) for test in obj.tests],
        }

    def encode_test_def(self, obj: t_defs.TestDef) -> Dict[str, Any]:
        """ Encode a single test definition. """
        return {
            'args': obj.args,
            'keys': obj.keys,
            'xform': obj.xform,
            'backend': obj.backend,
            'output': self.default(obj.output),
            'setenv': obj.setenv,
            'stdin': obj.stdin,
        }

    def encode_exact_output_def(self, obj: t_defs.TestExactOutputDef
                                ) -> Dict[str, str]:
        """ Encode an exact output requirement. """
        return {
            'exact': obj.exact,
        }

    def encode_exit_ok_output_def(self, obj: t_defs.TestExitOKOutputDef
                                  ) -> Dict[str, bool]:
        """ Encode an exit code requirement. """
        return {
            'exit': obj.success,
        }

    SERIALIZERS = {
        t_defs.TestFileDef: encode_test_file_def,
        t_defs.TestDef: encode_test_def,
        t_defs.TestExactOutputDef: encode_exact_output_def,
        t_defs.TestExitOKOutputDef: encode_exit_ok_output_def,
    }

    def default(self, obj: Any) -> Any:
        """ Meow. """
        method = self.SERIALIZERS.get(type(obj), None)
        if method is not None:
            return method(self, obj)
        return super(TestEncoder, self).default(obj)


def main() -> None:
    """ Main function: encode, output. """
    for name, tdef in sorted(t_data.TESTS.items()):
        print(f'--- {name} ---')
        with open(f'output/{name}.json', mode='w') as outf:
            print(json.dumps(tdef, indent=2, cls=TestEncoder), file=outf)


if __name__ == '__main__':
    main()
