#!/bin/sh
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'
[ -z "$TESTDIR" ] && TESTDIR='t'

echo '1..11'


if [ ! -f "$TESTDIR/t1.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t1.ini"
        exit 255
fi

if [ ! -f "$TESTDIR/t6.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t6.ini"
        exit 255
fi

if [ ! -f "$TESTDIR/t7.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t7.ini"
        exit 255
fi

if [ ! -f "$TESTDIR/t8.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t8.ini"
        exit 255
fi

if [ ! -f "$TESTDIR/t9.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t9.ini"
        exit 255
fi
v=`$CONFGET '-c' '-f' "$TESTDIR/t1.ini" '-s' a 'key1' `
res="$?"
if [ "$res" = 0 ]; then echo 'ok 1'; else echo "not ok 1 res is '$res'"; fi
v=`$CONFGET '-c' '-f' "$TESTDIR/t1.ini" '-s' a 'key2' `
res="$?"
if [ "$res" = 0 ]; then echo 'ok 2'; else echo "not ok 2 res is '$res'"; fi
v=`$CONFGET '-c' '-f' "$TESTDIR/t1.ini" '-s' a 'key3' `
res="$?"
if [ "$res" = 0 ]; then echo 'ok 3'; else echo "not ok 3 res is '$res'"; fi
v=`$CONFGET '-c' '-f' "$TESTDIR/t1.ini" '-s' a 'key4' `
res="$?"
if [ "$res" != 0 ]; then echo 'ok 4'; else echo "not ok 4 res is '$res'"; fi
v=`$CONFGET '-c' '-f' "$TESTDIR/t1.ini" '-s' 'b sect' 'key5' `
res="$?"
if [ "$res" != 0 ]; then echo 'ok 5'; else echo "not ok 5 res is '$res'"; fi
v=`$CONFGET '-c' '-f' "$TESTDIR/t1.ini" '-s' 'b sect' 'key4' `
res="$?"
if [ "$res" = 0 ]; then echo 'ok 6'; else echo "not ok 6 res is '$res'"; fi
v=`$CONFGET '-c' '-f' "$TESTDIR/t1.ini" '-s' c 'key5' `
res="$?"
if [ "$res" = 0 ]; then echo 'ok 7'; else echo "not ok 7 res is '$res'"; fi
v=`$CONFGET '-c' '-f' "$TESTDIR/t6.ini" 'any' `
res="$?"
if [ "$res" != 0 ]; then echo 'ok 8'; else echo "not ok 8 res is '$res'"; fi
v=`$CONFGET '-c' '-f' "$TESTDIR/t7.ini" 'any' `
res="$?"
if [ "$res" = 0 ]; then echo 'ok 9'; else echo "not ok 9 res is '$res'"; fi
v=`$CONFGET '-c' '-f' "$TESTDIR/t8.ini" 'any' `
res="$?"
if [ "$res" = 0 ]; then echo 'ok 10'; else echo "not ok 10 res is '$res'"; fi
v=`$CONFGET '-c' '-f' "$TESTDIR/t9.ini" 'any' `
res="$?"
if [ "$res" != 0 ]; then echo 'ok 11'; else echo "not ok 11 res is '$res'"; fi
