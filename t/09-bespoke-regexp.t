#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'
[ -z "$TESTDIR" ] && TESTDIR='t'

T1="$TESTDIR/t1.ini"

echo '1..8'

if [ ! -f "$T1" ]; then
	echo "Bail out!  No test file $T1"
	exit 255
fi

if ! $CONFGET -qfeatures | tr ' ' '\n' | grep -Eqe '^REGEX='; then
	for i in 1 2 3 4 5 6 7 8; do
		echo "ok $i # skip No regular expression support in confget"
	done
	exit 0
fi

unset cfg_key1 cfg_key2 cfg_key3 cfg_key6
eval `$CONFGET -f "$T1" -S -p cfg_ -s a -L -x 'key[23]'`
if [ "$cfg_key1" = '' ]; then echo 'ok 1'; else echo "not ok 1 cfg_key1 is '$cfg_key1'"; fi
if [ "$cfg_key2" = 'value2' ]; then echo 'ok 2'; else echo "not ok 2 cfg_key2 is '$cfg_key2'"; fi
if [ "$cfg_key3" = " val'ue3" ]; then echo 'ok 3'; else echo "not ok 3 cfg_key3 is '$cfg_key3'"; fi
if [ "$cfg_key6" = '' ]; then echo 'ok 4'; else echo "not ok 4 cfg_key6 is '$cfg_key6'"; fi

unset cfg_key1 cfg_key2 cfg_key3 cfg_key6
eval `$CONFGET -f "$T1" -S -p cfg_ -s a -L -m "'" -x 'key[23]'`
if [ "$cfg_key1" = '' ]; then echo 'ok 5'; else echo "not ok 5 cfg_key1 is '$cfg_key1'"; fi
if [ "$cfg_key2" = '' ]; then echo 'ok 6'; else echo "not ok 6 cfg_key2 is '$cfg_key2'"; fi
if [ "$cfg_key3" = " val'ue3" ]; then echo 'ok 7'; else echo "not ok 7 cfg_key3 is '$cfg_key3'"; fi
if [ "$cfg_key6" = '' ]; then echo 'ok 8'; else echo "not ok 8 cfg_key6 is '$cfg_key6'"; fi
