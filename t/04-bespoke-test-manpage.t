#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$MANPAGE" ] && MANPAGE='confget.1'
[ -z "$GROFF" ] && GROFF='groff'
[ -z "$GROFF_ARGS" ] && GROFF_ARGS='-mdoc -z'
[ -z "$ZCAT" ] && ZCAT='zcat'

echo '1..1'

if [ "${MANPAGE%.gz}" != "$MANPAGE" ]; then
	OUT=`$ZCAT $MANPAGE | $GROFF $GROFF_ARGS 2>&1`
else
	OUT=`$GROFF $GROFF_ARGS $MANPAGE 2>&1`
fi
if [ -z "$OUT" ]; then echo 'ok 1'; else echo 'not ok 1'; fi
