#!/bin/sh
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'
[ -z "$TESTDIR" ] && TESTDIR='t'

echo '1..14'


if [ ! -f "$TESTDIR/t3.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t3.ini"
        exit 255
fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" 'both' `
res="$?"
if [ "$v" = default ]; then echo 'ok 1'; else echo "not ok 1 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" 'defonly' `
res="$?"
if [ "$v" = default ]; then echo 'ok 2'; else echo "not ok 2 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" 'aonly' `
res="$?"
if [ "$v" = '' ]; then echo 'ok 3'; else echo "not ok 3 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" '-s' '' 'both' `
res="$?"
if [ "$v" = default ]; then echo 'ok 4'; else echo "not ok 4 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" '-s' '' 'defonly' `
res="$?"
if [ "$v" = default ]; then echo 'ok 5'; else echo "not ok 5 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" '-s' '' 'aonly' `
res="$?"
if [ "$v" = '' ]; then echo 'ok 6'; else echo "not ok 6 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" '-s' a 'both' `
res="$?"
if [ "$v" = a ]; then echo 'ok 7'; else echo "not ok 7 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" '-s' a 'defonly' `
res="$?"
if [ "$v" = '' ]; then echo 'ok 8'; else echo "not ok 8 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" '-s' a 'aonly' `
res="$?"
if [ "$v" = a ]; then echo 'ok 9'; else echo "not ok 9 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" '-s' a '-O' 'both' `
res="$?"
if [ "$v" = a ]; then echo 'ok 10'; else echo "not ok 10 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" '-s' a '-O' 'defonly' `
res="$?"
if [ "$v" = default ]; then echo 'ok 11'; else echo "not ok 11 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" '-s' a '-O' 'aonly' `
res="$?"
if [ "$v" = a ]; then echo 'ok 12'; else echo "not ok 12 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" '-m' 'def*' '-s' a '-O' 'both' `
res="$?"
if [ "$v" = '' ]; then echo 'ok 13'; else echo "not ok 13 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t3.ini" '-m' 'a*' '-s' a '-O' 'both' `
res="$?"
if [ "$v" = a ]; then echo 'ok 14'; else echo "not ok 14 v is '$v'"; fi
