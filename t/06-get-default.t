#!/bin/sh
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'
[ -z "$TESTDIR" ] && TESTDIR='t'

echo '1..5'


if [ ! -f "$TESTDIR/t1.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t1.ini"
        exit 255
fi

if [ ! -f "$TESTDIR/t2.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t2.ini"
        exit 255
fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" 'key1' `
res="$?"
if [ "$v" = value1 ]; then echo 'ok 1'; else echo "not ok 1 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" 'key4' `
res="$?"
if [ "$v" = '' ]; then echo 'ok 2'; else echo "not ok 2 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" 'key6' `
res="$?"
if [ "$v" = value6 ]; then echo 'ok 3'; else echo "not ok 3 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t2.ini" 'key1' `
res="$?"
if [ "$v" = 1 ]; then echo 'ok 4'; else echo "not ok 4 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t2.ini" 'key1' 'key2' | tr "\n" " "`
res="$?"
if [ "$v" = 'key1=1 key2=2 ' ]; then echo 'ok 5'; else echo "not ok 5 v is '$v'"; fi
