#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'

echo '1..11'

# First, make sure '-q features' reports 'BASE=...'
all="$($CONFGET -q features)"
if [ -n "$all" ]; then
	echo 'ok 1'
else
	echo 'not ok 1 -q features did not output anything'
fi

if [ "${all%%=*}" = 'BASE' ]; then
	base_and_all="${all#BASE=}"
else
	base_and_all="${all#* BASE=}"
fi
if [ "$base_and_all" != "$all" ]; then
	echo 'ok 2'
else
	echo "not ok 2 -q features did not contain 'BASE=': $all"
fi

just_base="${base_and_all%% *}"
if [ -n "$just_base" ]; then
	echo 'ok 3'
else
	echo "not ok 3 -q features contained an empty 'BASE=': $all"
fi

second_base="${base_and_all#* BASE=}"
if [ "$base_and_all" = "$second_base" ]; then
	echo 'ok 4'
else
	echo "not ok 4 -q features contained more than one 'BASE=': $all"
fi

# Now check that '-q feature BASE' outputs BASE
base="$($CONFGET -q feature BASE)"
if [ "$base" = "$just_base" ]; then
	echo 'ok 5'
else
	echo "not ok 5 -q feature BASE did not return the same string as in -q features: '$just_base' vs '$base'"
fi

# Now for some failure cases...
res=0
out="$($CONFGET -q features something 2>/dev/null)" || res="$?"
if [ "$res" -ne 0 ]; then
	echo 'ok 6'
else
	echo "not ok 6 '-q features' with an argument succeeded"
fi
if [ "$out" = '' ]; then
	echo 'ok 7'
else
	echo "not ok 7 '-q features' with an argument output something: $out"
fi

res=0
out="$($CONFGET -q feature 2>/dev/null)" || res="$?"
if [ "$res" -ne 0 ]; then
	echo 'ok 8'
else
	echo "not ok 8 '-q feature' with no argument succeeded"
fi
if [ "$out" = '' ]; then
	echo 'ok 9'
else
	echo "not ok 9 '-q feature' with no argument output something: $out"
fi

res=0
out="$($CONFGET -q feature BASE something 2>/dev/null)" || res="$?"
if [ "$res" -ne 0 ]; then
	echo 'ok 10'
else
	echo "not ok 10 '-q feature' with two arguments succeeded"
fi
if [ "$out" = '' ]; then
	echo 'ok 11'
else
	echo "not ok 11 '-q feature' with two arguments output something: $out"
fi
