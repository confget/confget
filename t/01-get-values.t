#!/bin/sh
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'
[ -z "$TESTDIR" ] && TESTDIR='t'

echo '1..16'


if [ ! -f "$TESTDIR/t1.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t1.ini"
        exit 255
fi

if [ ! -f "$TESTDIR/t4.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t4.ini"
        exit 255
fi

if [ ! -f "$TESTDIR/t6.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t6.ini"
        exit 255
fi

if [ ! -f "$TESTDIR/t7.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t7.ini"
        exit 255
fi

if [ ! -f "$TESTDIR/t8.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t8.ini"
        exit 255
fi

if [ ! -f "$TESTDIR/t9.ini" ]; then
        echo "Bail out!  No test file $TESTDIR/t9.ini"
        exit 255
fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a 'key1' `
res="$?"
if [ "$v" = value1 ]; then echo 'ok 1'; else echo "not ok 1 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a '-N' 'key2' `
res="$?"
if [ "$v" = key2=value2 ]; then echo 'ok 2'; else echo "not ok 2 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a 'key3' `
res="$?"
if [ "$v" = '		 val'"'"'ue3' ]; then echo 'ok 3'; else echo "not ok 3 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' 'b sect' 'key4' `
res="$?"
if [ "$v" = 'v'"'"'alu'"'"'e4' ]; then echo 'ok 4'; else echo "not ok 4 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' c 'key5' `
res="$?"
if [ "$v" = '		# value5' ]; then echo 'ok 5'; else echo "not ok 5 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a 'key1' 'key2' | tr "\n" " "`
res="$?"
if [ "$v" = 'key1=value1 key2=value2 ' ]; then echo 'ok 6'; else echo "not ok 6 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' a '-n' 'key6' 'key2' | tr "\n" " "`
res="$?"
if [ "$v" = 'value2 value6 ' ]; then echo 'ok 7'; else echo "not ok 7 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' 'b sect' 'key7' `
res="$?"
if [ "$v" = value7 ]; then echo 'ok 8'; else echo "not ok 8 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t1.ini" '-s' longcont 'long' `
res="$?"
if [ "$v" = 'stuff 	and more stuff	and even more stuff 	and wheee' ]; then echo 'ok 9'; else echo "not ok 9 v is '$v'"; fi
v=`$CONFGET '-s' 'b sect' -f - 'key7' < "$TESTDIR/t1.ini" `
res="$?"
if [ "$v" = value7 ]; then echo 'ok 10'; else echo "not ok 10 v is '$v'"; fi
v=`$CONFGET '-s' 'b sect' -f - 'key77' < "$TESTDIR/t1.ini" `
res="$?"
if [ "$v" = '' ]; then echo 'ok 11'; else echo "not ok 11 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t4.ini" '-s' x 'key8' `
res="$?"
if [ "$v" = key9=key10=key11 ]; then echo 'ok 12'; else echo "not ok 12 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t6.ini" 'any' 'none' `
res="$?"
if [ "$v" = '' ]; then echo 'ok 13'; else echo "not ok 13 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t7.ini" 'any' 'none' `
res="$?"
if [ "$v" = any=key ]; then echo 'ok 14'; else echo "not ok 14 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t8.ini" 'any' 'none' `
res="$?"
if [ "$v" = any=key ]; then echo 'ok 15'; else echo "not ok 15 v is '$v'"; fi
v=`$CONFGET '-f' "$TESTDIR/t9.ini" 'any' 'none' `
res="$?"
if [ "$v" = '' ]; then echo 'ok 16'; else echo "not ok 16 v is '$v'"; fi
