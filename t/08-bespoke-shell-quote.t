#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$CONFGET" ] && CONFGET='./confget'
[ -z "$TESTDIR" ] && TESTDIR='t'

T1="$TESTDIR/t1.ini"

echo '1..7'

if [ ! -f "$T1" ]; then
	echo "Bail out!  No test file $T1"
	exit 255
fi

v=`$CONFGET -f "$T1" -Ss a key1`
if [ "$v" = "'value1'" ] || [ "$v" = 'value1' ]; then echo 'ok 1'; else echo "not ok 1 v is '$v'"; fi
v=`$CONFGET -f "$T1" -Ss a key3`
if [ "$v" = "'		 val'\"'\"'ue3'" ] || [ "$v" = "'		 val'\\''ue3'" ]; then echo 'ok 2'; else echo "not ok 2 v is '$v'"; fi

unset cfg_key1 cfg_key2 cfg_key3 cfg_key6
eval `$CONFGET -f "$T1" -S -p cfg_ -s a -l`
if [ "$cfg_key1" = 'value1' ]; then echo 'ok 3'; else echo "not ok 3 cfg_key1 is '$cfg_key1'"; fi
if [ "$cfg_key2" = 'value2' ]; then echo 'ok 4'; else echo "not ok 4 cfg_key2 is '$cfg_key2'"; fi
if [ "$cfg_key3" = " val'ue3" ]; then echo 'ok 5'; else echo "not ok 5 cfg_key3 is '$cfg_key3'"; fi
if [ "$cfg_key6" = 'value6' ]; then echo 'ok 6'; else echo "not ok 6 cfg_key6 is '$cfg_key6'"; fi

unset cfg_key1_value
eval `$CONFGET -f "$T1" -S -p cfg_ -P _value -s a -N key1`
if [ "$cfg_key1_value" = 'value1' ]; then echo 'ok 7'; else echo "not ok 7 cfg_key1_value is '$cfg_key1_value'"; fi
